from fastapi import Depends, APIRouter
from queries.activity import ActivityIn, ActivityOut, ActivityQueries

router = APIRouter()


@router.get("/api/activities/", response_model=list[ActivityOut] | dict)
def activity_list(queries: ActivityQueries = Depends()):
    return queries.get_all_activities()


@router.post("/api/activities/", response_model=list[ActivityOut] | dict)
def create_activity(activity: ActivityIn, repo: ActivityQueries = Depends()):
    repo.create_activity(activity)
    return activity


@router.delete("/api/activities/{activity_id}", response_model=bool)
def delete_activity(
    activity_id: int,
    accounts: ActivityQueries = Depends(),
):
    accounts.delete_activity(activity_id)
    return True


@router.put("/api/activities/{activity_id}", response_model=ActivityOut)
def update_activity(
    activity_id: int,
    activity: ActivityIn,
    accounts: ActivityQueries = Depends(),
):
    record = accounts.update_activity(activity_id, activity)
    return record


@router.get("/api/activities/{activity_id}", response_model=ActivityOut)
def get_activity(
    activity_id: int,
    queries: ActivityQueries = Depends(),
) -> ActivityOut:
    activity = queries.get_one_activity(activity_id)
    if activity:
        return activity
    else:
        return ActivityOut(**{"message": "Activity not found"})
