from fastapi import Depends, APIRouter
from queries.reviews import ReviewIn, ReviewOut, ReviewQueries

router = APIRouter()


@router.get("/api/reviews/", response_model=list[ReviewOut] | dict)
def review_list(queries: ReviewQueries = Depends()):
    return queries.get_all_reviews()


@router.post("/api/reviews/", response_model=list[ReviewOut] | dict)
def create_review(
    review: ReviewIn,
    repo: ReviewQueries = Depends(),
):
    repo.create_review(review)
    return review


@router.delete("/api/reviews/{review_id}", response_model=bool)
def delete_review(
    review_id: int,
    accounts: ReviewQueries = Depends(),
):
    accounts.delete_review(review_id)
    return True


@router.put("/api/reviews/{review_id}", response_model=ReviewOut)
def update_review(
    review_id: int,
    review: ReviewIn,
    accounts: ReviewQueries = Depends(),
):
    record = accounts.update_review(review_id, review)
    return record


@router.get("/api/reviews/{review_id}", response_model=ReviewOut)
def get_review(
    review_id: int,
    queries: ReviewQueries = Depends(),
) -> ReviewOut:
    review = queries.get_one_review(review_id)
    if review:
        return review
    else:
        return None
