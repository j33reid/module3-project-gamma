from pydantic import BaseModel
from queries.pool import pool


class Error(BaseModel):
    message: str


class DuplicateAccountError(ValueError):
    pass


class AccountIn(BaseModel):
    username: str
    email: str
    password: str
    # favorites: list


class AccountOut(BaseModel):
    id: str
    username: str
    email: str
    # favorites: list


class AccountOutWithPassword(AccountOut):
    hashed_password: str


class AccountQueries:
    def get_all_accounts(self) -> list[AccountOut] | dict:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    find = cur.execute(
                        """
                        SELECT id, username, email
                        FROM accounts;
                        """
                    )

                    result_list = []
                    for record in find:
                        account = AccountOut(
                            id=record[0],
                            username=record[1],
                            email=record[2],
                            # hashed_password=record[3],
                        )
                        result_list.append(account)

                    return result_list
        except Exception:
            return {"message": "Error could not retrieve account user"}

    def create(
        self, account: AccountIn, hashed_password: str
    ) -> AccountOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    find = db.execute(
                        """
                        INSERT INTO accounts (username, email, hashed_password)
                        VALUES (%s, %s, %s)
                        RETURNING id, username, email, hashed_password;
                        """,
                        [account.username, account.email, hashed_password],
                    )
                    id = find.fetchone()[0]
                    old_data = account.dict()
                    return AccountOutWithPassword(
                        id=id, hashed_password=hashed_password, **old_data
                    )
        except Exception:
            return {"message": "Could not create user"}

    def get_account(self, email: str) -> AccountOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    find = db.execute(
                        """
                        SELECT id, username, email, hashed_password
                        FROM accounts
                        WHERE email = %s;
                        """,
                        [email],
                    )
                    record = find.fetchone()
                    if record is None:
                        return None
                    return AccountOutWithPassword(
                        id=record[0],
                        username=record[1],
                        email=record[2],
                        hashed_password=record[3],
                    )
        except Exception:
            return {"message": "Could not get account"}
