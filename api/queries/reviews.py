from pydantic import BaseModel
from queries.pool import pool


class ReviewIn(BaseModel):
    review: str
    rating: int
    activity_id: int


class ReviewOut(ReviewIn):
    id: int


class HttpError(BaseModel):
    pass


class ReviewQueries:
    def create_review(self, review: ReviewIn) -> ReviewOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    review.review,
                    review.rating,
                    review.activity_id,
                ]
                cur.execute(
                    """
                    INSERT INTO reviews (review, rating, activity_id)
                    VALUES (%s, %s, %s)
                    RETURNING id, review, rating, activity_id
                    """,
                    params,
                )
                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                return record

    def get_all_reviews(self) -> list[ReviewOut] | dict:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, review, rating, activity_id
                    FROM reviews;
                    """
                )
                results = []
                for row in cur.fetchall():
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                    results.append(record)
                return results

    def get_one_review(self, id: int) -> ReviewOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id, review, rating, activity_id
                    FROM reviews
                    WHERE id = %s;
                    """,
                    [id],
                )
                record = None
                row = db.fetchone()
                if row:
                    record = {}
                    for i, column in enumerate(db.description):
                        record[column.name] = row[i]
                return record

    def delete_review(self, review_id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        DELETE FROM reviews
                        WHERE id = %s
                        """,
                        [review_id],
                    )
        except Exception:
            return {"message": "Nothing to delete"}

    def update_review(self, review_id: int, review: ReviewIn):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    review.review,
                    review.rating,
                    review_id,
                ]
                cur.execute(
                    """
                    UPDATE reviews
                    SET review = %s, rating = %s
                    WHERE id = %s
                    RETURNING id, review, rating
                    """,
                    params,
                )
                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                return record
