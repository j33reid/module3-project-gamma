from pydantic import BaseModel
from queries.pool import pool


class ActivityIn(BaseModel):
    activity_name: str
    lat: float
    long: float
    activity_type: str
    hours: str
    description: str
    picture_url: str


class ActivityOut(ActivityIn):
    id: int


class HttpError(BaseModel):
    pass


class ActivityQueries:
    def create_activity(self, activity: ActivityIn) -> ActivityOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    activity.activity_name,
                    activity.lat,
                    activity.long,
                    activity.activity_type,
                    activity.hours,
                    activity.description,
                    activity.picture_url,
                ]
                cur.execute(
                    """
                    INSERT INTO activities (activity_name, lat, long,
                    activity_type, hours, description, picture_url)
                    VALUES (%s, %s, %s, %s, %s, %s, %s)
                    RETURNING id, activity_name, lat, long, activity_type,
                    hours, description, picture_url;
                    """,
                    params,
                )
                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                return record

    def get_all_activities(self) -> list[ActivityOut] | dict:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, activity_name, lat, long, activity_type, hours,
                    description, picture_url
                    FROM activities
                    ORDER BY activity_name;
                    """
                )
                results = []
                for row in cur.fetchall():
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                    results.append(record)
                return results

    def get_one_activity(self, activity_id: int) -> ActivityOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id, activity_name, lat, long, activity_type, hours,
                    description, picture_url
                    FROM activities
                    WHERE id = %s;
                    """,
                    [activity_id],
                )
                record = None
                row = db.fetchone()
                if row:
                    record = {}
                    for i, column in enumerate(db.description):
                        record[column.name] = row[i]
                return record

    def delete_activity(self, activity_id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        DELETE FROM activities
                        WHERE id = %s
                        """,
                        [activity_id],
                    )
        except Exception:
            return {"message": "Nothing to delete"}

    def update_activity(
        self, activity_id: int, activity: ActivityIn
    ) -> ActivityOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    activity.activity_name,
                    activity.lat,
                    activity.long,
                    activity.activity_type,
                    activity.hours,
                    activity.description,
                    activity_id,
                    activity.picture_url,
                ]
                cur.execute(
                    """
                    UPDATE activities
                    SET activity_name = %s,
                        lat = %s,
                        long = %s,
                        activity_type = %s,
                        hours = %s,
                        description = %s,
                        picture_url = %s,
                    WHERE id = %s
                    RETURNING id, activity_name, lat, long, activity_type,
                    hours, description, picture_url
                    """,
                    params,
                )
                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                return record
