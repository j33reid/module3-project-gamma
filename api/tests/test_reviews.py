from fastapi.testclient import TestClient
from main import app
from queries.reviews import ReviewIn, ReviewOut
from authenticator import authenticator
from fastapi.exceptions import HTTPException


client = TestClient(app)


def test_get_current_account_data():
    assert {
        "id": 0,
        "review": "string",
        "rating": "integer",
        "activity_id": "integer",
    }


class EmptyReviewRepo:
    def get_one_review(self, id: int) -> ReviewOut:
        if id == 0:
            return ReviewOut(
                id=0,
                review="string",
                rating="integer",
                activity_id="integer",
            )
        else:
            raise HTTPException(status_code=404, detail="Not Found")


test_review = {
    "id": 1,
    "review": "string",
    "rating": "integer",
    "activity_id": "integer",
}


def test_get_one_review():
    app.dependency_overrides[ReviewIn] = EmptyReviewRepo
    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = test_get_current_account_data

    response = client.get("/accounts/1")
    data = response.json()
    if response.status_code == 404:
        assert data["detail"] == "Not Found"
    else:
        assert response.status_code == 200
        assert data == {
            "id": 0,
            "review": "string",
            "rating": "integer",
            "activity_id": "integer",
        }

    app.dependency_overrides = {}
