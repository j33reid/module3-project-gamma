from fastapi.testclient import TestClient
from main import app
from queries.activity import ActivityOut, ActivityQueries
from authenticator import authenticator


client = TestClient(app)


def mock_get_current_account_data():
    assert {
        "id": 0,
        "activity_name": "Hiking",
        "lat": 35.6895,
        "long": 139.6917,
        "activity_type": "Outdoor",
        "hours": "10:00-18:00",
        "description": "Great outdoor experience.",
        "picture_url": "test.com",
    }


class MockActivityRepo:
    def get_all_activities(self):
        return [
            ActivityOut(
                id=0,
                activity_name="Hiking",
                lat=35.6895,
                long=139.6917,
                activity_type="Outdoor",
                hours="10:00-18:00",
                description="Great outdoor experience.",
                picture_url="test.com",
            ),
            ActivityOut(
                id=1,
                activity_name="Swimming",
                lat=35.6895,
                long=139.6917,
                activity_type="Outdoor",
                hours="09:00-17:00",
                description="Enjoy the water.",
                picture_url="test.com",
            ),
        ]


def test_get_all_activities():
    app.dependency_overrides[
        authenticator.get_account_data
    ] = mock_get_current_account_data
    app.dependency_overrides[ActivityQueries] = MockActivityRepo

    response = client.get("/api/activities/")
    data = response.json()

    assert response.status_code == 200
    assert len(data) == 2
    assert data[0]["activity_name"] == "Hiking"
    assert data[1]["activity_name"] == "Swimming"

    app.dependency_overrides = {}
