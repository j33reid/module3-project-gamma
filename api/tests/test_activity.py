from fastapi.testclient import TestClient
from main import app
from queries.activity import ActivityIn, ActivityOut
from authenticator import authenticator

client = TestClient(app)


def test_get_current_account_data():
    assert {
        "id": 0,
        "activity_name": "Mongo Aviation",
        "lat": 34.8963,
        "long": -85.446,
        "activity_type": "Human Lawn Darts",
        "hours": "sunup- sundown",
        "description": "Great time",
    }


class EmptyActivityRepo:
    def get_user(self, id: int) -> ActivityOut:
        if id == 0:
            return ActivityOut(
                id=0,
                activity_name="string",
                lat="float",
                long="float",
                activity_type="string",
                hours="string",
                description="string",
            )


test_account = {
    "id": 1,
    "activity_name": "string",
    "lat": "float",
    "long": "float",
    "activity_name": "string",
    "hours": "string",
}


def test_get_one_activity():
    app.dependency_overrides[ActivityIn] = EmptyActivityRepo
    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = test_get_current_account_data

    response = client.get("/activity/1")
    data = response.json()
    if response.status_code == 404:
        assert data["detail"] == "Not Found"
    else:
        assert response.status_code == 200
        assert data == {
            "id": 0,
            "activity_name": "string",
            "lat": "float",
            "long": "float",
            "activity_name": "string",
            "hours": "string",
        }

    app.dependency_overrides = {}
