steps = [
    [
        """
        CREATE TABLE accounts (
            id SERIAL PRIMARY KEY NOT NULL,
            username VARCHAR(100) NOT NULL UNIQUE,
            email VARCHAR(100) NOT NULL UNIQUE,
            hashed_password VARCHAR(100) NOT NULL
        );
        """,
        """
        DROP TABLE accounts;
        """,
    ],
    [
        """
        CREATE TABLE activities (
            id SERIAL PRIMARY KEY NOT NULL,
            activity_name VARCHAR(100) NOT NULL UNIQUE,
            lat FLOAT NOT NULL,
            long FLOAT NOT NULL,
            activity_type VARCHAR(100) NOT NULL,
            hours VARCHAR(100) NOT NULL,
            description VARCHAR(1000) NOT NULL,
            picture_url VARCHAR(200) NOT NULL
        );
        """,
        """
        DROP TABLE activities;
        """,
    ],
    [
        """
        CREATE TABLE reviews (
            id SERIAL PRIMARY KEY NOT NULL,
            review VARCHAR(1000) NOT NULL,
            rating INTEGER NOT NULL,
            activity_id INTEGER NOT NULL
        );
        """,
        """
        DROP TABLE reviews
        """,
    ],
]
