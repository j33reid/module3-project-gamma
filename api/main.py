from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
from authenticator import authenticator
from routers import accounts, activity, reviews


app = FastAPI()
app.include_router(authenticator.router, tags=["authenticator"])
app.include_router(accounts.router, tags=["accounts"])
app.include_router(activity.router, tags=["activity"])
app.include_router(reviews.router, tags=["reviews"])

app.add_middleware(
    CORSMiddleware,
    allow_origins=[os.environ.get("CORS_HOST", "http://localhost:3000")],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def read_root():
    return {"Hello": "World"}
