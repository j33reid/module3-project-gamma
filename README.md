# Wayfinder

## Developed By

    - Joseph Keene
    - Cyrus Yeung
    - Ricky Trang
    - Jason Anderson
    - Dillon Dewundara

## Overview

Wayfinder: paving the path to discovery, one review at a time.
Wayfinder: beyond location, into experience.
Wayfinder: your global guide, powered by local wisdom.

## Application Description

Wayfinder is an innovative web application engineered to connect individuals with their surroundings and with each other. The platform not only provides a mechanism to create, share, and access reviews of various locations globally, but also allows users to curate activities tied to specific GPS coordinates. In a world increasingly driven by digital interactions, Wayfinder is your key to making the physical world more interactive, more informative, and ultimately, more immersive.

## Key Features

User Authentication: Sign up, log in, and log out functionality. We value your security and privacy. Your data is secured with state-of-the-art encryption methods.
Creating an activity as well as creating a review for that activity.

## Location-based Reviews:

Review any activity on the planet based on your GPS coordinates.

## Activity Creation:

Add a new layer of depth to every location. With Wayfinder, you can create activities at any given site. It's your way to contribute to the global community and to the stories that make every place unique.

## Weather Data Retrieval:

Get real-time weather data at any location or activity. With this feature, you're never caught off guard by weather changes. Plan and adapt your activities with up-to-the-minute, accurate meteorological information.

## Interactive Mapping:

Drop a pin on our world map to add or locate activities and reviews. Visualize your next adventure with our intuitive, easy-to-navigate mapping system.

## Chatbot Functionality:

Never feel lost with our AI-powered chatbot ready to assist you at any time. It's your personal companion as you navigate the world of Wayfinder.

## Getting Started:

Experience the full potential of Wayfinder on your local machine by following these steps:

Clone the repository to your local machine.
Navigate into the project directory.
Execute the following commands in your terminal:

    docker volume create wayfinder-data
    docker-compose build
    docker-compose up
    docker exec -it wayfinder-api-1 bash

Exit the container's CLI.
Dive into the immersive world of Wayfinder!
This platform is built with the future in mind, continuously striving to enhance user experience and stay abreast of technological advancements. We are committed to the continuous improvement and expansion of Wayfinder, to better serve our users and the communities they create.

Wayfinder: Your world. Your way.

