### HOME PAGE MAP

* Endpoint path: /maps
* Endpoint method: GET

* Headers:

* Response: Map
* Response shape:
    ```json
    {
      "locations": [
        {
          "name": string,
          "description": string,
          "gps_location": string,
          "activity_type": string,
          "rating": number  might remove,
          "reviews": string?,
          "hours_operation": datetime,
          "image_url": url,
          "weather": api-string?,
          "is_favorite": boolean,
        }
      ]
    }
    ```



### GET LOCATION DETAIL

* Endpoint path: /maps/id
* Endpoint method: GET

* Headers:

* Response: Location Detail
* Response shape:
    ```json
    {
      "locations": [
        {
          "id": ?,
          "name": string,
          "description": string,
          "gps_location": string,
          "activity": string,
          "rating": number,
          "reviews": string?,
          "hours_operation": datetime,
          "image_url": url,
          "weather": api-string?,
          "is_favorite": boolean,
        }
      ]
    }
    ```



### UPDATE LOCATION DETAIL

* Endpoint path: /maps/id
* Endpoint method: PUT
<!-- * Query parameters:
  * q: locations.all()   We might need this if we want to have more
  complicated search's combining multiple paramaters in our search -->

* Headers:
  * Authorization: Bearer token

* Response: Map
* Response shape:
    ```json
    {
      "locations": [
        {
          "name": string,
          "description": string,
          "gps_location": string,
          "activity_type": string,
          "rating": number  might remove,
          "reviews": string?,
          "hours_operation": datetime,
          "image_url": url,
          "weather": api-string?,
          "is_favorite": boolean,
        }
      ]
    }
    ```

* Response: An indication of success or failure
* Response shape:
    ```json
    {
      "success": boolean,
      "message": string
    }
    ```



### DELETE LOCATION DETAIL

* Endpoint path: /maps/id
* Endpoint method: DELETE

* Headers:
  * Authorization: Bearer token

* Response: An indication of success or failure
* Response shape:
    ```json
    {
      "success": boolean,
      "message": string
    }




### CREATE A NEW REVIEW

* Endpoint path: /review/id
* Endpoint method: POST

* Headers:
  * Authorization: Bearer token

* Request body:
    ```json
    {
      "username": sting,
      "text": string,
      "image_url": string,
      "rating": number,
      "location_id": string,
    }
    ```

* Response: An indication of success or failure
* Response shape:
    ```json
    {
      "success": boolean,
      "message": string
    }
    ```



### LOCATION DETAIL REVIEW LIST

* Endpoint path: /location/id/review/
* Endpoint method: GET

* Headers:


* Response: Location Detail
* Response shape:
    ```json
    {
      "username": sting,
      "comments": string,
      "image_url": string,
      "rating": number,
    }
    ```


### USER REVIEW LIST

* Endpoint path: /location/review/
* Endpoint method: GET

* Headers:


* Response: Location Detail
* Response shape:
    ```json
    {
      "comments": string,
      "image_url": string,
      "rating": number,
    }
    ```





### UPDATE A REVIEW

* Endpoint path: /review/id
* Endpoint method: PUT

* Headers:
  * Authorization: Bearer token

* Request body:
    ```json
    {
      "text": string,
      "image_url": string,
      "rating": number,
    }
    ```

* Response: An indication of success or failure
* Response shape:
    ```json
    {
      "success": boolean,
      "message": string
    }
    ```



### DELETE A REVIEW

* Endpoint path: /review/id
* Endpoint method: DELETE

* Headers:
  * Authorization: Bearer token

* Response: An indication of success or failure
* Response shape:
    ```json
    {
      "success": boolean,
      "message": string
    }




### GET A LIST OF FAVORITE LOCATIONS

* Endpoint path: /maps/favorites
* Endpoint method: GET
* Query parameters:
  * q: locations.favorites would need if we added based on certain locations?

* Headers:
  * Authorization: Bearer token

* Response: A list of favorite locations
* Response shape:
    ```json
    {
      "locations": [
        {
            "name": string,
            "activity": string,
            "gps_location": api?,
            "rating": number,
        }
      ]
    }
    ```


### DELETE FAVORITE

* Endpoint path: /maps/favorites/id
* Endpoint method: DELETE

* Headers:
  * Authorization: Bearer token

* Response: An indication of success or failure
* Response shape:
    ```json
    {
      "success": boolean,
      "message": string
    }




### LOG IN

* Endpoint path: /token
* Endpoint method: POST

* Request shape (form):
  * username: string
  * password: string

* Response: Account information and a token
* Response shape (JSON):
    ```json
    {
      "account": {
        «key»: type»,
      },
      "token": string
    }
    ```


### LOG OUT

* Endpoint path: /token
* Endpoint method: DELETE

* Headers:
  * Authorization: Bearer token

* Response: Always true
* Response shape (JSON):
    ```json
    true
    ```


### SIGN UP

* Endpoint path: /token
* Endpoint method: POST

* Request shape (form):
  * username: string
  * password: string

* Response: Account information and a token
* Response shape (JSON):
    ```json
    {
        "signUp": [
            {
            "username": string,
            "email": string,
            "password": string,
            }
        ]
    }
    ```
