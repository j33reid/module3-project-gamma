import React, { useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";

function CreateActivityForm() {
    const [activity_name, setActivityName] = useState("");
    const [lat, setLat] = useState("");
    const [long, setLong] = useState("");
    const [activity_type, setActivityType] = useState("");
    const [hours, setHours] = useState("");
    const [description, setDescription] = useState("");
    const [pictureUrl, setPictureUrl] = useState("");
    const navigate = useNavigate();
    const { register } =useToken();


    const handleSubmit =  (e) => {
        e.preventDefault();
        const locationData = {
            activity_name: activity_name,
            lat: lat,
            long: long,
            activity_type: activity_type,
            hours: hours,
            description: description,
            picture_url: pictureUrl,

        };
        register(
            locationData,
             `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/api/activities`,
        );
        e.target.reset();
        navigate("/");

    };

return (
    <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1 id="subtitle" className="text-center">Create an Activity</h1>
            <form onSubmit={(e) => handleSubmit(e)}>
            <div className="form-floating mb-3">
                <input
                onChange={(e) => {
                    setActivityName(e.target.value);
                }}
                placeholder="activity_name"
                required
                type="text"
                name="activity_name"
                id="activity_name"
                className="form-control"
                />
                <label htmlFor="activity_name">Location Name</label>
            </div>
            <div className="form-floating mb-3">
                <input
                onChange={(e) => {
                    setLat(e.target.value);
                }}
                placeholder="latitude"
                required
                type="text"
                name="latitude"
                id="latitude"
                className="form-control"
                />
                <label htmlFor="name">Latitude</label>
            </div>
            <div className="form-floating mb-3">
                <input
                onChange={(e) => {
                    setLong(e.target.value);
                }}
                placeholder="longitude"
                required
                type="text"
                name="longitude"
                id="longitude"
                className="form-control"
                />
                <label htmlFor="name">Longitude</label>
            </div>
            <div className="form-floating mb-3">
                <input
                onChange={(e) => {
                    setActivityType(e.target.value);
                }}
                placeholder="activity_type"
                required
                type="text"
                name="activity_type"
                id="activity_type"
                className="form-control"
                />
                <label htmlFor="activity_type">Type of Activity</label>
            </div>
            <div className="form-floating mb-3">
                <input
                onChange={(e) => {
                    setHours(e.target.value);
                }}
                placeholder="hours"
                required
                type="text"
                name="hours"
                id="hours"
                className="form-control"
                />
                <label htmlFor="hours">Hours</label>
            </div>
            <div className="form-floating mb-3">
                <textarea
                onChange={(e) => {
                    setDescription(e.target.value);
                }}
                placeholder="review"
                required
                name="review"
                id="review"
                className="form-control"
                style={{ height: "400px" }}
                />
                <label htmlFor="review">Description</label>
            </div>
            <div className="form-floating mb-3">
                <input
                onChange={(e) => {
                    setPictureUrl(e.target.value);
                }}
                placeholder="picture_url"
                required
                type="text"
                name="picture_url"
                id="picture_url"
                className="form-control"
                />
                <label htmlFor="picture_url">Picture URL</label>
            </div>
            <div className="text-center">
                <button className="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
        </div>
    </div>
    );
}

export default CreateActivityForm;
