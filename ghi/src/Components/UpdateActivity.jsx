import React, { useState,useEffect } from "react";

function UpdateActivity() {
    const [activity_name, setActivityName] = useState("");
    const [lat, setLat] = useState("");
    const [long, setLong] = useState("");
    const [activity_type, setActivityType] = useState("");
    const [hours, setHours] = useState("");
    const [errorMessage, setErrorMessage] = useState('')

    const handleActivityChange = (event) => {
        const value = event.target.value
        setActivityName(value)
    }

    const handleLatChange = (event) => {
        const value = event.target.value
        setLat(value)
    }

    const handleLongChange = (event) => {
        const value = event.target.value
        setLong(value)
    }
    const handleActivityTypeChange = (event) => {
        const value = event.target.value
        setLat(value)
    }

    const handleHoursChange = (event) => {
        const value = event.target.value
        setHours(value)
    }


    const handleSubmit =  async (event) => {
        event.preventDefault();
        const data = {};

        data.activity_name
        data.lat
        data.long
        data.activity_type
        data.hours

        const url = 'http://localhost:8000/api/activities/'
        const config = {
            method: 'put',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(url, config)
        if(response.ok){
            setActivityName('')
            setLat('')
            setLong('')
            setActivityType('')
            setHours('')
        } else{
            const data = await response.json()
            setErrorMessage(data.message)
        }
    }
    





    return (
       <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Update an Activity</h1>
              <form onSubmit={handleSubmit} id="create-favorite">
                <div className="form-floating mb-3">
                  <input onChange={handleActivityChange} placeholder="Activity" required type="text" name="activity_name" id="activity_name" className="form-control" value={activity_name} />
                  <label htmlFor="activity">Activity</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleLatChange} placeholder="Latitude" required type="text" name="lat" id="lat" className="form-control" value={lat} />
                  <label htmlFor="latitude">Latitude</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleLongChange} placeholder="Longitude" required type="text" name="long" id="long" className="form-control" value={long} />
                  <label htmlFor="longitude">Longitube</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleActivityTypeChange} placeholder="Activity Type" required type="text" name="activity_type" id="activity_type" className="form-control" value={activity_type} />
                  <label htmlFor="activity_type">Activity Type</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleHoursChange} placeholder="Hours" required type="text" name="hours" id="hours" className="form-control" value={hours} />
                  <label htmlFor="hours">Hours</label>
                </div>
                <div>
                  {errorMessage && (
                    <div className="alert alert-danger" role="alert">
                      {errorMessage}
                    </div>
                  )}
                </div>
                <button className="btn btn-primary">Update</button>
              </form>
            </div>
          </div>
        </div> 
    )
}

export default UpdateActivity;