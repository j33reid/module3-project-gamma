import React, { useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";

const SignUpForm = () => {
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const { register } = useToken();
    const navigate = useNavigate();

    const handleRegistration = (e) => {
        e.preventDefault();
        const accountData = {
            username: username,
            email: email,
            password: password,
        };

        register(
            accountData,
            `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/api/accounts`,

        );
        e.target.reset();
        navigate("/login");
    };

return (
    <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1 id="subtitle" className="text-center">Create an Account</h1>
            <form onSubmit={(e) => handleRegistration(e)}>
                <div className="form-floating mb-3">
                    <input
                    onChange={(e) => {
                        setUsername(e.target.value);
                    }}
                    placeholder="username"
                    required
                    type="text"
                    name="username"
                    id="username"
                    className="form-control"
                    />
                <label htmlFor="first_name">User Name</label>
                </div>
                <div className="form-floating mb-3">
                <input
                    onChange={(e) => {
                        setEmail(e.target.value);
                    }}
                    placeholder="email"
                    required
                    type="email"
                    name="email"
                    id="email"
                    className="form-control"
                />
                <label htmlFor="email">Email</label>
                </div>
                <div className="form-floating mb-3">
                <input
                    onChange={(e) => {
                        setPassword(e.target.value);
                    }}
                    placeholder="password"
                    required
                    type="password"
                    name="password"
                    id="password"
                    className="form-control"
                />
                <label htmlFor="address">Password</label>
                </div>
                <div className="text-center">
                <button className="btn btn-primary">SignUp</button>
                </div>
            </form>
        </div>
        </div>
    </div>
);
}

export default SignUpForm;
