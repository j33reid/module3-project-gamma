import useToken from "@galvanize-inc/jwtdown-for-react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

const LoginForm = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const { login } = useToken();
  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();
    login(username, password);
    e.target.reset();
    navigate("/");
  };

return (
    <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1 className="text-center">Login to Your Account</h1>
            <form onSubmit={(e) => handleSubmit(e)}>
                <div className="form-floating mb-3">
                    <input onChange={(e) => setUsername(e.target.value)} placeholder="User Name" required type="text" name="username" value={username} id="username" className="form-control"/>
                    <label htmlFor="username">Email</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={(e) => setPassword(e.target.value)} placeholder="password" required type="password" name="password" value={password} id="password" className="form-control"/>
                    <label htmlFor="password">Password</label>
                </div>
                <div className="text-center">
                    <button className="btn btn-primary">Login</button>
                </div>
            </form>
        </div>
        </div>
    </div>
);
}

export default LoginForm;
