import React, { useState } from "react";


function UpdateReview() {
    const [activity_name, setActivityName] = useState("");
    const [rating, setRating] = useState("");
    const [review, setReview] = useState("");
    const [errorMessage, setErrorMessage] = useState('')
    

    const handleActivityChange = (event) => {
        const value = event.target.value
        setActivityName(value)
    }

    const handleRatingChange = (event) => {
        const value = event.target.value
        setRating(value)
    }

    const handleReviewChange = (event) => {
        const value = event.target.value
        setReview(value)
    }

    const handleSubmit =  async (event) => {
        event.preventDefault();
        const data = {};

        data.activity_name
        data.rating
        data.review

        const url = 'http://localhost:8000/api/reviews/'
        const config = {
            method: 'put',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(url, config)
        if(response.ok){
            setActivityName('')
            setRating('')
            setReview('')
        } else{
            const data = await response.json()
            setErrorMessage(data.message)
        }
    }






    return (
       <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Update an Activity</h1>
              <form onSubmit={handleSubmit} id="create-favorite">
                <div className="form-floating mb-3">
                  <input onChange={handleActivityChange} placeholder="Activity" required type="text" name="activity_name" id="activity_name" className="form-control" value={activity_name} />
                  <label htmlFor="activity">Activity</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleRatingChange} placeholder="Rating" required type="text" name="rating" id="rating" className="form-control" value={rating} />
                  <label htmlFor="rating">Rating</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleReviewChange} placeholder="Review" required type="text" name="review" id="review" className="form-control" value={review} />
                  <label htmlFor="review">Review</label>
                </div>
                <div>
                  {errorMessage && (
                    <div className="alert alert-danger" role="alert">
                      {errorMessage}
                    </div>
                  )}
                </div>
                <button className="btn btn-primary">Update</button>
              </form>
            </div>
          </div>
        </div> 
    )
}

export default UpdateReview;