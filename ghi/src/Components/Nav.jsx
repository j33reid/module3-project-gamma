import useToken from "@galvanize-inc/jwtdown-for-react";
import { NavLink } from 'react-router-dom';
import './Nav.css';

function Nav() {
  const { token, logout } = useToken();

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid">
        <NavLink to="/" className="no-blue-underline"><h1>WayFinder</h1></NavLink>
        <h4 className="white-title"> Your go to solution for finding activities</h4>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav ms-auto">
            {token && (
              <>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/activities">Create an Activity</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/CreateReview">Create a Review</NavLink>
                </li>
              </>
            )}
            <li className="nav-item">
              <NavLink className="nav-link" to="/favorites">Favorites</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/signup">SignUp</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/login">Login</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/logout" onClick={logout}>
                Logout
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/chat">Chat</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
