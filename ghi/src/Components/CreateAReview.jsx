import React, { useState, useEffect } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";

function CreateReviewForm() {
  const [activity, setActivity] = useState("");
  const [activities, setActivities] = useState([]);
  const [rating, setRating] = useState("");
  const [review, setReview] = useState("");
  const navigate = useNavigate();
  const { register } = useToken();

  const fetchActivities = async () => {
    const url = `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/api/activities/`;
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setActivities(data);
    }
  };

  const handleChangeActivity = (e) => {
    setActivity(e.target.value);
  };

  const handleChangeRating = (e) => {
    setRating(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const reviewData = {
      activity_id: parseInt(activity, 10),
      rating: parseInt(rating, 10),
      review: review,
    };
    register(reviewData, `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/api/reviews`);
    e.target.reset();
    navigate("/");
  };


  useEffect(() => {
    fetchActivities();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1 id="subtitle" className="text-center">Write a review</h1>
          <form onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <select onChange={handleChangeActivity} id="activity_name" className="form-select">
                <option value="">Choose an activity</option>
                {activities?.map((activity) => (
                  <option key={activity.id} value={activity.id}>
                    {activity.activity_name}
                  </option>
                ))}
              </select>
              <label htmlFor="activity_name">Activity</label>
            </div>
            <div className="form-floating mb-3">
              <select
                onChange={handleChangeRating}
                id="rating"
                className="form-select"
                value={rating}
              >
                <option value="">Choose a rating</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
              </select>
              <label htmlFor="rating">Rating</label>
            </div>
            <div className="form-floating mb-3">
              <textarea
                onChange={(e) => {
                  setReview(e.target.value);
                }}
                placeholder="review"
                required
                name="review"
                id="review"
                className="form-control"
                style={{ height: "400px" }}
              />
              <label htmlFor="review">Review</label>
            </div>
            <div className="text-center">
              <button className="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CreateReviewForm;
