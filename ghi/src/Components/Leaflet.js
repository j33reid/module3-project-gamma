import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import {
  MapContainer,
  TileLayer,
  Marker,
  Popup,
} from "react-leaflet";
import "leaflet/dist/leaflet.css";
import "./Leaflet.css";
import L from 'leaflet';

const markerIcon = new L.Icon({
  iconUrl: require("./images/map_marker.jpeg"),
  iconSize: [35,35],
});

function MapRender() {
  const [activities, setActivities] = useState([]);

  const getWeatherDescription = (tempFahrenheit) => {
    if (tempFahrenheit > 95) {
      return "Kind of hot";
    } else if (tempFahrenheit >= 80) {
      return "Warm";
    } else if (tempFahrenheit >= 65) {
      return "Cool";
    } else {
      return "Freezing";
    }
  }

  useEffect(() => {
  const fetchMaps = async() => {
    const response = await fetch(
      `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/api/activities/`
    );

    const data = await response.json();

    const activitiesWithTemperature = await Promise.all(data.map(async activity => {
      const lat = activity.lat;
      const lon = activity.long;

      let apiKey = '3gpQKzmSBOa23c23hYEqrPMlakvEnYzE';
      let url = `https://api.tomorrow.io/v4/timelines?location=${lat},${lon}&fields=temperature&timesteps=current&units=metric&apikey=${apiKey}`;

      const weatherResponse = await fetch(url);
      const weatherData = await weatherResponse.json();

      if (weatherData && weatherData.data && weatherData.data.timelines) {
        let weatherCelsius = weatherData.data.timelines[0].intervals[0].values.temperature;
        let weatherFahrenheit = (weatherCelsius * 9/5) + 32;
        let weatherDescription = getWeatherDescription(weatherFahrenheit);

        activity.temperature = weatherFahrenheit;
        activity.weatherDescription = weatherDescription;
      }
      return activity;
    }));
    setActivities(activitiesWithTemperature);
  }
    fetchMaps();
  }, []);

  return (
    <MapContainer
      center={[40.0902, -97.7129]}
      zoom={5}
      scrollWheelZoom={true}
      maxBounds={[]}
    >
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      {activities && activities.map((activity, idx) => (
        <NavLink key={activity.id} to={`/activities/${activity.id}`}>
          <Marker position={[activity.lat, activity.long]} key={idx} icon={markerIcon}>
            <Popup className="pin-image">
              <b>
                Activity Name: {activity.activity_name}
                <br/>
                Activity Type: {activity.activity_type}
                <br/>
                Description: {activity.description}
                <br/>
                Temperature: {activity.temperature}°F - {activity.weatherDescription}
              </b>
            </Popup>
          </Marker>
        </NavLink>
      ))}
    </MapContainer>
  );
}

export default MapRender;
