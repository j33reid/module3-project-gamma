import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import SignUpForm from "./Components/SignUpForm.jsx";
import Nav from "./Components/Nav.jsx";
import LoginForm from "./Components/LoginForm.jsx";
import LogoutForm from "./Components/Logout.jsx";
import MapRender from "./Components/Leaflet.js";
import "./App.css";
import LocationDetail from "./LocationDetail.js";
import CreateReviewForm from "./Components/CreateAReview.jsx";
import CreateActivityForm from "./Components/CreateActivity.jsx";
import Chat from "./chatbot/chatbot.jsx"


function App() {
  const domain = /https:\/\/[^/]+/;
  const baseUrl = `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}`;
  const basename = process.env.PUBLIC_URL.replace(domain, '');

  return (
    <BrowserRouter basename={basename}>
      <AuthProvider baseUrl={baseUrl}>
        <Nav />
        <div className="container">
          <Routes>
            <Route path="/" element={<MapRender />} />
            <Route path="/signup" element={<SignUpForm />} />
            <Route path="/login" element={<LoginForm />} />
            <Route path="/logout" element={<LogoutForm />} />
            <Route path="/createreview" element={<CreateReviewForm />} />
            <Route path="/activities" element={<CreateActivityForm />} />
            <Route path="/activities/:id" element={<LocationDetail />} />
            <Route path="/chat" element={<Chat />} />
          </Routes>
        </div>
      </AuthProvider>
    </BrowserRouter>
  );
}

export default App;
