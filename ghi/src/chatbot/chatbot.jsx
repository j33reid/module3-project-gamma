import React from "react";
import Chatbot from 'react-chatbot-kit';

import config from "./chatbotConfig";
import MessageParser from "./MessageParser";
import ActionProvider from "./ActionProvider";
import 'react-chatbot-kit/build/main.css';

function Chat() {
    return (
        <div className="chat">
            <div>
                <Chatbot 
                config={config}
                messageParser={MessageParser}
                actionProvider={ActionProvider}
                />
        </div>
        </div>
    )
} 

export default Chat;