import { createChatBotMessage } from 'react-chatbot-kit';

const botName = "Baxter"

const config = {
    initialMessages: [createChatBotMessage(`Hello, Im ${botName}! How can I help today?`)],
    botName: "Baxter",
    customStyles: {
    botMessageBox: {
      backgroundColor: '#376B7E',
    },
    chatButton: {
      backgroundColor: '#5ccc9d',
    },
  }, 
}

export default config;