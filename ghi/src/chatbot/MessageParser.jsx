import React from 'react';

const MessageParser = ({ children, actions }) => {
  const parse = (message) => {
    if (message.includes('activity')) {
      actions.handleActivity();
    }
    else if (message.includes('map')) {
      actions.handleView();
    }
    else if (message.includes('review')) {
      actions.handleReview();
    }
    else {
      actions.handleUnknown();
    }
  };

  return (
    <div>
      {React.Children.map(children, (child) => {
        return React.cloneElement(child, {
          parse: parse,
          actions,
        });
      })}
    </div>
  );
};

export default MessageParser;