### 5/8/2023

Came to a decision to call our App WayFinder. Our unofficial team name is called Ricky's Angels.
Today we came up with an idea for people who enjoy the outdoors. We wanted to have it show an interactive map that allowed people to see points of interests in terms of activities.

### 5/9/2023

Wireframing was started. So far we have created the wire framing for a main page, a log in form, and a sign up form. Location detail page was also started wire framing as a place to list and describe activites.
began work on ratings page

### 5/10/2023

Continued wireframing

### 5/11/2023

Resume day, little work was done on the project

### 5/12/2023

Team work on api-end points issues
each member was assigned issues

### 5/15/2023

docker compose.yml started

### 5/16/2023

Accounts file created
authentication was created

### 5/17/2023

Login, Log Out, and sign up are working
Having issues with the docker compose.yaml but solved them

### 5/22/2023

we have divided the project so that each member can work seperatly.
im working on the css and assisting with the backend

### 5/23/2023

Worked on the map home page and getting it to render properly

### 5/24/2023

I have started working on a new feature that i want our project to include. Im have no idea how to do it and this could be a waste of time but i think a chatbot would be a unique addtion to our site

### 5/24/2023

I spent most of the reading the react documentation on chatbot intergration via react. Surprisingly, it does not require any backend work so I have to work on a small set of newly created files

### 5/25/2023

Created a message parser react file to look for keyworks when a users types in the prompt. also created an action provider while tells the chat bot how to respond to key words. Created the Configure file for the bot and the action chat bot render itself.

### 5/26/2023

It seems like any sort of AI intergration is going to extremely difficult. I want the chatbot to have more functionality than simple phrase responses to key words but unless I can figure out /find AI chatbot intergration within the 24 hours, im going to move on

### 5/29/2023

Ok so I fixed the chat box and it works now. I have probably spent way too much time on it but im done and it works

### 6/1/2023

I forgot to journal the past few days. Today we got the login and logout functions to work

### 6/2/2023

Sign up is a little broken due to simple naimg issues. but those have been corrected

### 6/5/2023

Created the favorites page and the ablity to view and create favorites. Doesnt work yet but the framework is mostly done, wasnt able to finish but spent all day trying to get this to work

### 6/6/2023

Kicked into high gear today, I spent all of today working on creating update functionality for both the activities. The issues im currently running into has todo with the register function which is used in the create activity jsx file. Register function uses a POST method so the set up is diiferent from what im used to seeing. In order to create an activity, log in is required. If update is in a seperate file could it be set up without the token?

### 6/7/2023

The update function for activities does work with forcefed JSON data but intergration into the app is proving to be more difficult. moved on to creating the update review file. but once again, im am stumped on how to integrate the changes on to the location detail page. because there is no review page, im not sure we will even need this.
Created the my first merge request with in gitlab today even though i had been pushing and merging earlier

### 6/8/2023

worked on creating tests for our functionality for reviews and activities

### 6/9/2023

Worked on getting deployed, fixing bugs with our tests and code and worked on adding more functionality to the chatbot
