Kept track of journal on google docs instead of MD files. Didn’t want to merge everytime I updated the file after.

6/9/2023

Resolved a problem in the details page. Also finished deployment with the group.

6/8/2023

Worked on making sure the detail page rendered the correct data based on each reviews associated acitivty_id. Worked a bit more on the deployment code.

6/7/2023

Fixed the map so that it renders activity data as pins from the backend, problem was we were trying to access data.activities but all we needed was just data. Started working on the deployment code.

6/6/2023

Worked with Ricky and Jason to resolve errors in the backend, finally fixed and added the ability to create new activities and reviews. Started trying to render backend data on the frontend map, was not working as of writing.

6/5/2023

Remade the backend to better reflect our apps functionality, changed locations to activity and removed the third-party api call as our api call is now front-end exclusive. Also added the backend framework for reviews. Made a get accounts unit test for use later.

6/1/2023 - 6/2/2023

Looked into alternatives for third-party api cools for the weather feature of our app. Dillion’s choice of tomorrow.io was decided for our api call.

5/31/2023

Had a serious discussion with the group to narrow down on functionality we want. I continued to work on making the backend endpoints show on swagger.

5/30/2023

Errors are still there with the login/logout/create account front end pages. Tried to resolve it on the backend butended the day with little progress.

5/26/2023 - 5/29/2023

Went back to elaborate on backend end-points for map/location.

5/25/2023

Created a dummy json file with possible data on what we might pull from the backend, used to make pins render on the map. Also worked with Ricky to get a detail page to reflect the data in the json file.

5/24/2023

Resolved the map rendering issue, the problem stemmed from an issue with the css. Tried to resolve issues with the signup form but kept getting a 422 error.

5/23/2023

Spent the entirety of the day trying to get react-leaflet to render a map on the page. Setup the react portion of the map, and later worked with Ricky and Jason to render a broken version of the map.

5/22/2023

Completed the routers for map/location over the last two days, also setup the backend queries and routers for a potential third-party api call. Group still undecided on what third-party API to use.

5/18/2023 - 5/19/2023

I continued to set up the framework for the backend, I started making the queries and routers for the location/map table.

5/17/2023

Backend requests regarding the accounts were not working on swagger, but these were resolved by the end of the day. However, we did resolve our issues pertaining to docker. I also began setting up the skeleton for different queries and routers.

5/16/2023

The authenticator file was made along with queries for the accounts portion of the project.

5/15/2023

Made the docker.yaml file, and made sure appropriate containers were spun up.

5/12/2023

Finished the endpoints, and made issues for each individual member to accomplish throughout the project.

5/11/2023

Spent some time working on finalizing the resume, spent the remainder working on drafting out the endpoints for the backend.

5/10/2023

Continued reading up on potential API’s, settling on leaflet to render the map but still deciding on the weather API to use.

5/9/2023

Wireframing was mostly completed today. Ricky used figma instead of excalidraw to do most of the wireframing. I spent the remainder of the day reading up on potential API’s to use.

5/8/2023

Was late to the group start due to internet issues, but the app we decided to work on is called Wayfinder. The app is an interactive map meant to showcase activities and hobbies for people to do in the area. Still deciding on what API to call.
