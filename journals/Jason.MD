# Journal

### 6/9/2023

With the help of fellow students we were able to push through and somehow get succefully deployed. We had issues all day long that we worked through.  Andrew was taking meeting for a lot of the day but as our cohort learned they all worked together to get everyone deployed.  It was really great, our group would not have made it!!!!

### 6/8/2023

I foccussed on deployment, while everyone else was quickly making our project functional as we planned it.  Able to create activities, leave a rating and review.  Ricky and I worked on cicd till closing time, think my last merge was after 12pm.

### 6/7/2023

We were finnally able to focus on the front end with our backend functioning, while also able to create table's in our migrations folder which sped up our progress.

### 6/6/2023

We focused on the backend finishing all the functionality so that everything worked in swagger.  Also I was ready to give up on the idea of a migrations table working but we switched to beekeeper and were successful.

### 6/5/2023

We realized our backend was not functioning properly and Ricky, Cyrus and I spent all day working through functions

### 6/4/2023

We got together saturday and sunday, foccused on creating an acctivity

### 6/2/2023

Ricky drove, we spent most of the day sorting through connection errors between the front and backend and getting a better understanding of how everything is working together.

### 6/1/2023

I drove, we were able to signup on our react front end page, I added logout in the navbar.  I tried to make it so that the login or logout button would hide if the user was logged in or out but didn't work.

### 5/31/2023

we worked on the front end foccussing on the login and signup functions

### 5/30/2023

A review page was made, I was able to create an account and login

### 5/29/2023

I worked on authentication, further exploring axios still on the fence and the navbar was made.

### 5/26/2023

Continued to work on authentication, not sure whether to use axios or not.  Some people are not using the jwt token with more documentation and instruction/how to.

### 5/25/2023

Ricky drove and live shared, I have been working on the front end authentication so far but only issues.  Still can't create an account, Cyrus was able to get some location data to show up on the map.

### 5/24/2023

I drove and live shared, still unable to signup an account but we were able to get our map to render.

### 5/23/2023

I drove and liveshared, we worked on the map and signup account.

### 5/22/2023

I drove and live shared, finished our create accounts function but doesn't work.

### 5/18/2023

I drove and used liveshare for the first time, we started building the front end.  Started the signup and login forms.

### 5/17/2023

Cyrus and Ricky drove, we created tables today or learned how to making mistakes along the way. We are able to login, logout, create an account, and get a token.

### 5/16/2023

Cyrus and Joseph were our drivers today, created Pool.py, continued working on our backend working on accounts.

### 5/15/2023

Ricky and I split driving and were able to get our backend running, worked on our yaml file

### 5/12/2023

Dillon drove, continued working on our api backend.

### 5/11/2023

I was the driver, we got started on our api backend writing endpoints.

### 5/10/2023

This was a resume day, we did make some detail changes to our wireframe, Ricky was the driver.

### 5/9/2023

Ricky did all the driving today on our wireframing, we have a main page not signed in where you can still use the map but maybe not the search functions?  A main page signed in where you can sort by activity type and possibly have a radar/weather overlay.  We want locations to pop up detail information, we will have a signup page, a login page, a review page, maybe a user_review page.  Also we want the Users to be able to create a location.

### 5/8/2023

We came to a decision fairly quickly for our project to be a large map of the United States that you could zoom into different area's to find different activities.  We are going to focus on outdoor activites that we could use as a filter sort.  We want to be able to review these sites and possibly have a forum, we are calling the site WayFinder, and our team name is Ricky's Angels.

## WEEK 1
